# Copyright (C) 2024 The Qt Company Ltd.
# SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only

MACHINE_EXTRA_INSTALL_SDK += "libnss-mdns"
