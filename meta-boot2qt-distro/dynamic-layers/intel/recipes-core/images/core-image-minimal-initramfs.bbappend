
INITRAMFS_SCRIPTS += "\
    initramfs-module-rtsx-pci-sdmmc \
    initramfs-module-network \
    "
PACKAGE_INSTALL += "\
    kernel-module-rtsx-pci-sdmmc \
    kernel-module-r8169 \
    kernel-module-igc \
    kernel-module-realtek \
    linux-firmware-rtl8168 \
    "
