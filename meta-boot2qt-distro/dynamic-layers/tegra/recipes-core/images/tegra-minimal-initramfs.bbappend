PACKAGE_INSTALL += " \
    nv-kernel-module-nvethernet \
    nv-kernel-module-r8126 \
    nv-kernel-module-r8168 \
    ${@bb.utils.contains("DISTRO_FEATURES", "nfs", \
        "nfs-utils-client nfs-utils-mount", "", d)} \
    "
