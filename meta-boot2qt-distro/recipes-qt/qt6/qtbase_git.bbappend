
PACKAGECONFIG += " \
    ctf \
    cups \
    glib \
    sql-sqlite \
    tslib \
    xkbcommon \
    "

do_configure:prepend() {
    echo "QMAKE_PLATFORM += boot2qt" >> ${S}/mkspecs/oe-device-extra.pri
}

EXTRA_OECMAKE:remove = "-DQT_AVOID_CMAKE_ARCHIVING_API=ON"
